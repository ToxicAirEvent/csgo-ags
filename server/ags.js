const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const NodeCache = require('node-cache');

const app = express(express.json());
const gsCache = new NodeCache();

//Parse whatever data comes our way.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post('/', async function (req, res) {

  console.log("New gamestate data!");

  let gameData = req.body;

  console.log(gameData.provider);
  console.log(gameData.map);
  console.log(gameData.player);
  console.log(gameData.player.weapons)

  let formatGameData = await matchData(gameData);

  /*
    Once we've formatted the data the way we want it to make it easier to use in our application, and only what we need set the cache.
    The cache can always just be set. No need to see if there is one existing already as we're just trying to perpetuate it forward with fresh data.
  */
  await gsCache.set('gameStateLog', formatGameData, 1450);
  
  await playerDetails(gameData);

  return res.status(200);
})

app.get('/data', async function (req, res){

  console.log("Cache Stats:");
  console.log(gsCache.getStats());

  if(!gsCache.has('gameStateLog') || !gsCache.has('playerStats')){
    return res.json({match: 'idle', players: false});
  }

  const matchData = await gsCache.get('gameStateLog');
  const playerStats = await gsCache.get('playerStats');

  return res.json({match: matchData, players: playerStats});
})

async function matchData(gsData){

  const mapData = gsData.map;

  /*
    If there is no map data or the players name is 'unconnected' then they aren't in a game.
    We want to flag this so the logic is a bit more clean in the front end display.

    We'll also need to return this since no map data will be present.

    Not that 'unconnected' could be a problem is someone actually names themselves exactly that.
    Fairly unlikely though.
  */
  if(!gsData.map || gsData.player.name === 'unconnected'){
    return {clientIs: 'idle'};
  }

  let reformat = {
    clientIs: 'playing',
    round: mapData.round,
    round_history: mapData.round,
    mode: mapData.mode,
    map: mapData.name,
    phase: mapData.phase,
    score_ct: mapData.team_ct,
    score_t: mapData.team_t,
  }

  return reformat;
}

async function playerDetails(gsData){

  //Make an empty player stats object. If some are already stored we'll go ahead and grab the data we've already got.
  let playerStats = {};

  if(gsCache.has("playerStats")){
    playerStats = await gsCache.get('playerStats');
    console.log("Retrieved player stats.");
  }

  let steamID = gsData.player.steamid;

  playerStats[steamID] = gsData.player;

  await gsCache.set('playerStats',playerStats, 1450);

  return playerStats;
}

app.listen(3192)
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const NodeCache = require('node-cache');

const app = express(express.json());
const gsCache = new NodeCache();

//Parse whatever data comes our way.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.get('/data', async function (req, res){

  console.log("Cache Stats:");
  console.log(gsCache.getStats());

  if(!gsCache.has('matchStats') || !gsCache.has('playerStats')){
    return res.json({match: 'idle', players: false});
  }

  const matchData = await gsCache.get('matchStats');
  const playerStats = await gsCache.get('playerStats');

  return res.json({match: matchData, players: playerStats});
})

app.post('/', async function (req, res) {

  //Get the body of the request with gamestate integration data and cast it to a easily callable variable.
  const gameData = req.body;

  console.log("New gamestate data!");
  console.log(gameData.provider);
  console.log(gameData.map);
  console.log(gameData.player);
  console.log(gameData.player.weapons)

  if(!gameData.map){
    console.log("Not in a game. Doing nothing.");
    return null;
  }

  //Create an empty object of match data for any matches currently under observation.
  let match_data = {};

  //If there are already matches under observation pull them out and cast them to the match data objects.
  if(gsCache.has('matchStats')){
    match_data = await gsCache.get('matchStats');
  }

  //Create a state key for the match under observation for the current gamestate input. This will allow multiple gamestates to be observed without collision.
  const mgm_key = await createGameKey(gameData);
  match_data[mgm_key] = await formatMatchData(gameData);

  //Dump into and extend the cache data for both match info and player info.
  await gsCache.set('matchStats', match_data, 10450);
  await playerDetails(gameData);

  return res.status(200);
})

async function formatMatchData(gsData){

  const mapData = gsData.map;

  /*
    If there is no map data or the players name is 'unconnected' then they aren't in a game.
    We want to flag this so the logic is a bit more clean in the front end display.

    We'll also need to return this since no map data will be present.

    Not that 'unconnected' could be a problem is someone actually names themselves exactly that.
    Fairly unlikely though.
  */
  if(!gsData.map || gsData.player.name === 'unconnected'){
    return {clientIs: 'idle'};
  }

  let reformat = {
    clientIs: 'playing',
    round: mapData.round,
    round_history: mapData.round,
    mode: mapData.mode,
    map: mapData.name,
    phase: mapData.phase,
    score_ct: mapData.team_ct,
    score_t: mapData.team_t,
  }

  return reformat;
}

async function playerDetails(gsData){

  //Make an empty player stats object. If some are already stored we'll go ahead and grab the data we've already got.
  let playerStats = {};

  if(gsCache.has("playerStats")){
    playerStats = await gsCache.get('playerStats');
  }

  let steamID = gsData.player.steamid;

  //Set parity tracking fields.
  gsData.player['observed_by'] = gsData.provider.steamid;
  gsData.player['mgm_key'] = await createGameKey(gsData);

  playerStats[steamID] = gsData.player;

  await gsCache.set('playerStats',playerStats, 10450);

  return playerStats;
}

async function createGameKey(gsData){
  const mgm_key = `${gsData.map.name}_${gsData.map.mode}`;

  return mgm_key;
}

app.listen(3192)